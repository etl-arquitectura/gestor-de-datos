##!/usr/bin/env python
# -*- coding: utf-8 -*-
#-------------------------------------------------------------------------
# Archivo: txt_transformer.py
# Capitulo: Flujo de Datos
# Autor(es): Ulises Huerta, Juan Navarro, Alexis Ultreras y Reyna Sanchez
# Version: 2.0.0 Marzo 2023
# Descripción:
#
#   Este archivo define un procesador de datos que se encarga de transformar
#   y formatear el contenido de un archivo TXT
#-------------------------------------------------------------------------

from src.extractors.txt_extractor import TXTExtractor
from os.path import join
import luigi, os, json

class TXTTransformer(luigi.Task):

    def requires(self):
        return TXTExtractor()

    def run(self):
        result = []
        for file in self.input():
            with file.open() as txt_file:
                datos = [i.strip().split(",") for a in txt_file for i in a.split(";")]
                for pos in range(1,len(datos)-1):
                    linea = datos[pos]
                    result.append(
                        {
                        "description": linea[2],
                        "quantity": linea[3],
                        "price": linea[5],
                        "total": float(linea[5]),
                        "invoice": linea[0],
                        "provider": linea[6],
                        "country": linea[7],
                        "date":linea[4]
                        }
                    )

        with self.output().open('w') as out:
            out.write(json.dumps(result, indent=4))

    def output(self):
        project_dir = os.path.dirname(os.path.abspath("loader.py"))
        result_dir = join(project_dir, "result")
        return luigi.LocalTarget(join(result_dir, "txt.json"))